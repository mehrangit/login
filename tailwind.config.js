const colors = require('tailwindcss/colors');
module.exports = {
  purge: [
    './src/**/*.html',
    './src/**/*.js',
  ],
  darkMode: 'class', // or 'media' or 'class'
  theme: {
    extend: {
      animation: {
        'wiggle': 'wiggle 1s linear infinite',
      },
      colors: {
        'gray': colors.gray
      },
      boxShadow: {
        glass: 'inset 0 2px 22px 0 rgba(255, 255, 255, 0.6);',
        darkglass: 'inset 0 2px 22px 0 rgba(0, 0, 0, 0.6);',
      },
      keyframes: {
        wiggle: {
          '0%, 100%': { transform: 'rotate(-9deg)' },
          '50%': { transform: 'rotate(9deg)' },
        }
      },
    },
  },
  variants: {
    extend: {
      brightness: ['hover'],
      transitionProperty: ['dark'],
      boxShadow: ['dark'],
      backgroundImage: ['dark'],
      backgroundColor: ['dark'],
    },
  },
  plugins: [],
}
